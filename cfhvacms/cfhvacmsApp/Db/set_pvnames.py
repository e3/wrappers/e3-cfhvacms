# Pwr-N1U02:CnPw-Q-001:Voltage-RB
# pattern {RECORD_TYPE,PV_NAME,SUBS,NODE_ID,DESC}
import re

f = open("./hvac.subst", "r")
o = open("./output.subst", "w")
for line in f:
    elements = line.split(",")
    if elements[0].startswith("{") and len(elements) > 1:
        pv_elements = elements[1].split(":")
        systemsubsystem = pv_elements[0].split("-")
        device = systemsubsystem[1][3:]
        m = re.search(r"\d", device)
        if m:
            deviceindex = device[m.start() :]
            device = device[0 : m.start()]
            systemsubsystem[1] = systemsubsystem[1][0:3]
            pvname = "-".join(systemsubsystem) + ":HVAC-" + device + "-" + deviceindex

            if pv_elements[2] == "TR-RB":
                pv_elements[2] = "TempRoom-R"
            if pv_elements[2] == "TRAzo1-RB":
                pv_elements[2] = "TempAzo1-R"
            if pv_elements[2] == "TRAzo2-RB":
                pv_elements[2] = "TempAzo2-R"
            elif pv_elements[2] == "Alm-RB":
                pv_elements[2] = "Fault"
            elif pv_elements[2] == "Alm0-RB":
                pv_elements[2] = "Fault0"
            elif pv_elements[2] == "Alm1-RB":
                pv_elements[2] = "Fault1"
            elif pv_elements[2] == "CmnAlm-RB":
                pv_elements[2] = "FaultCmn"
            elif pv_elements[2] == "CmnAlm2-RB":
                pv_elements[2] = "FaultCmn2"
            elif pv_elements[2] == "TEx-RB":
                pv_elements[2] = "TempEx-R"
            elif pv_elements[2] == "TR1-RB":
                pv_elements[2] = "TempRoom1-R"
            elif pv_elements[2] == "TR2-RB":
                pv_elements[2] = "TempRoom2-R"
            elif pv_elements[2] == "TR3-RB":
                pv_elements[2] = "TempRoom3-R"
            elif pv_elements[2] == "TR4-RB":
                pv_elements[2] = "TempRoom4-R"
            elif pv_elements[2] == "TR5-RB":
                pv_elements[2] = "TempRoom5-R"

            pvname = pvname + ":" + pv_elements[2]
            elements[1] = pvname

    newline = ",".join(elements)
    o.write(newline)
f.close()
o.close()
