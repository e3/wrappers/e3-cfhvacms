This directory contains a cfhvacms test UA server and a client tool.
The cfhvacmsTestServer will be launched to do tests when a "make test" command is issued at the top directory of e3-cfhvacms module after build and cellinstall.

One can also run the cfhvacmsTestServer directly as following:

	$ cd /path/to/e3-cfhvacms/test/server
	$ make
	$ ./cfhvacmsTestServer

In oder to make changes to the test server you will need to install the open62541 project locally.

        $ git clone --recurse-submodules https://github.com/open62541/open62541.git

You can then rebuild the server files using

        $ make nodeset

and build the server using (or let e3 do it by running make test from the top folder in e3-cfhvacms)

        $ make 

