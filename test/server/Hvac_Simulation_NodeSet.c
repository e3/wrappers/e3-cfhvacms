/* WARNING: This is a generated file.
 * Any manual changes will be overwritten. */

#include "Hvac_Simulation_NodeSet.h"


/* Simulation - ns=2;s=85/0:Simulation */

static UA_StatusCode function_Hvac_Simulation_NodeSet_0_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "Simulation");
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_STRING(ns[2], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 85LU),
UA_NODEID_NUMERIC(ns[0], 35LU),
UA_QUALIFIEDNAME(ns[1], "Simulation"),
UA_NODEID_NUMERIC(ns[0], 58LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
if (retVal != UA_STATUSCODE_GOOD) return retVal;
return retVal;
}

static UA_StatusCode function_Hvac_Simulation_NodeSet_0_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[2], "85/0:Simulation")
);
}

/* System1.OPC:OPC.H06.A.Ahu200.FanEh.Cmd.Feedback_Value - ns=2;s=System1.OPC:OPC.H06.A.Ahu200.FanEh.Cmd.Feedback01_Value */

static UA_StatusCode function_Hvac_Simulation_NodeSet_1_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.historizing = true;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 6LU);
UA_Int32 *variablenode_ns_2_s_system1_opc_opc_h06_a_ahu200_faneh_cmd_feedback01_value_variant_DataContents =  UA_Int32_new();
if (!variablenode_ns_2_s_system1_opc_opc_h06_a_ahu200_faneh_cmd_feedback01_value_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Int32_init(variablenode_ns_2_s_system1_opc_opc_h06_a_ahu200_faneh_cmd_feedback01_value_variant_DataContents);
*variablenode_ns_2_s_system1_opc_opc_h06_a_ahu200_faneh_cmd_feedback01_value_variant_DataContents = (UA_Int32) 0;
UA_Variant_setScalar(&attr.value, variablenode_ns_2_s_system1_opc_opc_h06_a_ahu200_faneh_cmd_feedback01_value_variant_DataContents, &UA_TYPES[UA_TYPES_INT32]);
attr.displayName = UA_LOCALIZEDTEXT("", "System1.OPC:OPC.H06.A.Ahu200.FanEh.Cmd.Feedback_Value");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "System1.OPC:OPC.H06.A.Ahu200.FanEh.Cmd.Feedback_Value");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[2], "System1.OPC:OPC.H06.A.Ahu200.FanEh.Cmd.Feedback01_Value"),
UA_NODEID_STRING(ns[2], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "System1.OPC:OPC.H06.A.Ahu200.FanEh.Cmd.Feedback_Value"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
if (retVal != UA_STATUSCODE_GOOD) return retVal;
UA_Int32_delete(variablenode_ns_2_s_system1_opc_opc_h06_a_ahu200_faneh_cmd_feedback01_value_variant_DataContents);
return retVal;
}

static UA_StatusCode function_Hvac_Simulation_NodeSet_1_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[2], "System1.OPC:OPC.H06.A.Ahu200.FanEh.Cmd.Feedback01_Value")
);
}

/* System1.OPC:OPC.G02.Process.EC003.IAR.PT021.PV.Value - ns=2;s=System1.OPC:OPC.G02.Process.EC003.IAR.PT021.PV.Value */

static UA_StatusCode function_Hvac_Simulation_NodeSet_2_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_2_s_system1_opc_opc_g02_process_ec003_iar_pt021_pv_value_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_2_s_system1_opc_opc_g02_process_ec003_iar_pt021_pv_value_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_2_s_system1_opc_opc_g02_process_ec003_iar_pt021_pv_value_variant_DataContents);
*variablenode_ns_2_s_system1_opc_opc_g02_process_ec003_iar_pt021_pv_value_variant_DataContents = (UA_Double) 6.86863422393799;
UA_Variant_setScalar(&attr.value, variablenode_ns_2_s_system1_opc_opc_g02_process_ec003_iar_pt021_pv_value_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "System1.OPC:OPC.G02.Process.EC003.IAR.PT021.PV.Value");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "System1.OPC:OPC.G02.Process.EC003.IAR.PT021.PV.Value");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[2], "System1.OPC:OPC.G02.Process.EC003.IAR.PT021.PV.Value"),
UA_NODEID_STRING(ns[2], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[2], "System1.OPC:OPC.G02.Process.EC003.IAR.PT021.PV.Value"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
if (retVal != UA_STATUSCODE_GOOD) return retVal;
UA_Double_delete(variablenode_ns_2_s_system1_opc_opc_g02_process_ec003_iar_pt021_pv_value_variant_DataContents);
return retVal;
}

static UA_StatusCode function_Hvac_Simulation_NodeSet_2_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[2], "System1.OPC:OPC.G02.Process.EC003.IAR.PT021.PV.Value")
);
}

/* System1.OPC:OPC.G02.Process.EC003.DIW.FT009.PV.Value - ns=2;s=System1.OPC:OPC.G02.Process.EC003.DIW.FT009.PV.Value */

static UA_StatusCode function_Hvac_Simulation_NodeSet_3_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_2_s_system1_opc_opc_g02_process_ec003_diw_ft009_pv_value_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_2_s_system1_opc_opc_g02_process_ec003_diw_ft009_pv_value_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_2_s_system1_opc_opc_g02_process_ec003_diw_ft009_pv_value_variant_DataContents);
*variablenode_ns_2_s_system1_opc_opc_g02_process_ec003_diw_ft009_pv_value_variant_DataContents = (UA_Double) 0.0;
UA_Variant_setScalar(&attr.value, variablenode_ns_2_s_system1_opc_opc_g02_process_ec003_diw_ft009_pv_value_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "System1.OPC:OPC.G02.Process.EC003.DIW.FT009.PV.Value");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "System1.OPC:OPC.G02.Process.EC003.DIW.FT009.PV.Value");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[2], "System1.OPC:OPC.G02.Process.EC003.DIW.FT009.PV.Value"),
UA_NODEID_STRING(ns[2], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[2], "System1.OPC:OPC.G02.Process.EC003.DIW.FT009.PV.Value"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
if (retVal != UA_STATUSCODE_GOOD) return retVal;
UA_Double_delete(variablenode_ns_2_s_system1_opc_opc_g02_process_ec003_diw_ft009_pv_value_variant_DataContents);
return retVal;
}

static UA_StatusCode function_Hvac_Simulation_NodeSet_3_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[2], "System1.OPC:OPC.G02.Process.EC003.DIW.FT009.PV.Value")
);
}

/* System1.OPC:OPC.G02.Process.EC001.CWH.FT001.PV.Value - ns=2;s=System1.OPC:OPC.G02.Process.EC001.CWH.FT001.PV.Value */

static UA_StatusCode function_Hvac_Simulation_NodeSet_4_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.historizing = true;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_2_s_system1_opc_opc_g02_process_ec001_cwh_ft001_pv_value_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_2_s_system1_opc_opc_g02_process_ec001_cwh_ft001_pv_value_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_2_s_system1_opc_opc_g02_process_ec001_cwh_ft001_pv_value_variant_DataContents);
*variablenode_ns_2_s_system1_opc_opc_g02_process_ec001_cwh_ft001_pv_value_variant_DataContents = (UA_Double) 42.4714279174805;
UA_Variant_setScalar(&attr.value, variablenode_ns_2_s_system1_opc_opc_g02_process_ec001_cwh_ft001_pv_value_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "System1.OPC:OPC.G02.Process.EC001.CWH.FT001.PV.Value");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "System1.OPC:OPC.G02.Process.EC001.CWH.FT001.PV.Value");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[2], "System1.OPC:OPC.G02.Process.EC001.CWH.FT001.PV.Value"),
UA_NODEID_STRING(ns[2], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "System1.OPC:OPC.G02.Process.EC001.CWH.FT001.PV.Value"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
if (retVal != UA_STATUSCODE_GOOD) return retVal;
UA_Double_delete(variablenode_ns_2_s_system1_opc_opc_g02_process_ec001_cwh_ft001_pv_value_variant_DataContents);
return retVal;
}

static UA_StatusCode function_Hvac_Simulation_NodeSet_4_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[2], "System1.OPC:OPC.G02.Process.EC001.CWH.FT001.PV.Value")
);
}

/* System1.OPC:OPC.G02.Process.EC001.CWH.CV004.PV.Value - ns=2;s=System1.OPC:OPC.G02.Process.EC001.CWH.CV004.PV.Value */

static UA_StatusCode function_Hvac_Simulation_NodeSet_5_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.historizing = true;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_2_s_system1_opc_opc_g02_process_ec001_cwh_cv004_pv_value_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_2_s_system1_opc_opc_g02_process_ec001_cwh_cv004_pv_value_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_2_s_system1_opc_opc_g02_process_ec001_cwh_cv004_pv_value_variant_DataContents);
*variablenode_ns_2_s_system1_opc_opc_g02_process_ec001_cwh_cv004_pv_value_variant_DataContents = (UA_Double) 8.25376129150391;
UA_Variant_setScalar(&attr.value, variablenode_ns_2_s_system1_opc_opc_g02_process_ec001_cwh_cv004_pv_value_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "System1.OPC:OPC.G02.Process.EC001.CWH.CV004.PV.Value");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "System1.OPC:OPC.G02.Process.EC001.CWH.CV004.PV.Value");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[2], "System1.OPC:OPC.G02.Process.EC001.CWH.CV004.PV.Value"),
UA_NODEID_STRING(ns[2], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "System1.OPC:OPC.G02.Process.EC001.CWH.CV004.PV.Value"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
if (retVal != UA_STATUSCODE_GOOD) return retVal;
UA_Double_delete(variablenode_ns_2_s_system1_opc_opc_g02_process_ec001_cwh_cv004_pv_value_variant_DataContents);
return retVal;
}

static UA_StatusCode function_Hvac_Simulation_NodeSet_5_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[2], "System1.OPC:OPC.G02.Process.EC001.CWH.CV004.PV.Value")
);
}

/* System1.OPC:OPC.G02.Process.EC001.CWH.CT010.PV.Value - ns=2;s=System1.OPC:OPC.G02.Process.EC001.CWH.CT010.PV.Value */

static UA_StatusCode function_Hvac_Simulation_NodeSet_6_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.historizing = true;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_2_s_system1_opc_opc_g02_process_ec001_cwh_ct010_pv_value_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_2_s_system1_opc_opc_g02_process_ec001_cwh_ct010_pv_value_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_2_s_system1_opc_opc_g02_process_ec001_cwh_ct010_pv_value_variant_DataContents);
*variablenode_ns_2_s_system1_opc_opc_g02_process_ec001_cwh_ct010_pv_value_variant_DataContents = (UA_Double) 0.10416666418314;
UA_Variant_setScalar(&attr.value, variablenode_ns_2_s_system1_opc_opc_g02_process_ec001_cwh_ct010_pv_value_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "System1.OPC:OPC.G02.Process.EC001.CWH.CT010.PV.Value");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "System1.OPC:OPC.G02.Process.EC001.CWH.CT010.PV.Value");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[2], "System1.OPC:OPC.G02.Process.EC001.CWH.CT010.PV.Value"),
UA_NODEID_STRING(ns[2], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "System1.OPC:OPC.G02.Process.EC001.CWH.CT010.PV.Value"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
if (retVal != UA_STATUSCODE_GOOD) return retVal;
UA_Double_delete(variablenode_ns_2_s_system1_opc_opc_g02_process_ec001_cwh_ct010_pv_value_variant_DataContents);
return retVal;
}

static UA_StatusCode function_Hvac_Simulation_NodeSet_6_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[2], "System1.OPC:OPC.G02.Process.EC001.CWH.CT010.PV.Value")
);
}

/* System1.OPC:OPC.G02.Process.EC001.CWH.CT009.PV.Value - ns=2;s=System1.OPC:OPC.G02.Process.EC001.CWH.CT009.PV.Value */

static UA_StatusCode function_Hvac_Simulation_NodeSet_7_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.historizing = true;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_2_s_system1_opc_opc_g02_process_ec001_cwh_ct009_pv_value_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_2_s_system1_opc_opc_g02_process_ec001_cwh_ct009_pv_value_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_2_s_system1_opc_opc_g02_process_ec001_cwh_ct009_pv_value_variant_DataContents);
*variablenode_ns_2_s_system1_opc_opc_g02_process_ec001_cwh_ct009_pv_value_variant_DataContents = (UA_Double) 0.119285300374031;
UA_Variant_setScalar(&attr.value, variablenode_ns_2_s_system1_opc_opc_g02_process_ec001_cwh_ct009_pv_value_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "System1.OPC:OPC.G02.Process.EC001.CWH.CT009.PV.Value");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "System1.OPC:OPC.G02.Process.EC001.CWH.CT009.PV.Value");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[2], "System1.OPC:OPC.G02.Process.EC001.CWH.CT009.PV.Value"),
UA_NODEID_STRING(ns[2], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "System1.OPC:OPC.G02.Process.EC001.CWH.CT009.PV.Value"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
if (retVal != UA_STATUSCODE_GOOD) return retVal;
UA_Double_delete(variablenode_ns_2_s_system1_opc_opc_g02_process_ec001_cwh_ct009_pv_value_variant_DataContents);
return retVal;
}

static UA_StatusCode function_Hvac_Simulation_NodeSet_7_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[2], "System1.OPC:OPC.G02.Process.EC001.CWH.CT009.PV.Value")
);
}

/* System1.OPC:OPC.G01.A.Ahu400.Azo.TR.Present_Value - ns=2;s=System1.OPC:OPC.G01.A.Ahu400.Azo.TR.Present_Value */

static UA_StatusCode function_Hvac_Simulation_NodeSet_8_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_2_s_system1_opc_opc_g01_a_ahu400_azo_tr_present_value_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_2_s_system1_opc_opc_g01_a_ahu400_azo_tr_present_value_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_2_s_system1_opc_opc_g01_a_ahu400_azo_tr_present_value_variant_DataContents);
*variablenode_ns_2_s_system1_opc_opc_g01_a_ahu400_azo_tr_present_value_variant_DataContents = (UA_Double) 18.6312255859375;
UA_Variant_setScalar(&attr.value, variablenode_ns_2_s_system1_opc_opc_g01_a_ahu400_azo_tr_present_value_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "System1.OPC:OPC.G01.A.Ahu400.Azo.TR.Present_Value");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "System1.OPC:OPC.G01.A.Ahu400.Azo.TR.Present_Value");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[2], "System1.OPC:OPC.G01.A.Ahu400.Azo.TR.Present_Value"),
UA_NODEID_STRING(ns[2], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[2], "System1.OPC:OPC.G01.A.Ahu400.Azo.TR.Present_Value"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
if (retVal != UA_STATUSCODE_GOOD) return retVal;
UA_Double_delete(variablenode_ns_2_s_system1_opc_opc_g01_a_ahu400_azo_tr_present_value_variant_DataContents);
return retVal;
}

static UA_StatusCode function_Hvac_Simulation_NodeSet_8_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[2], "System1.OPC:OPC.G01.A.Ahu400.Azo.TR.Present_Value")
);
}

/* System1.OPC:OPC.F03.A.Ahu101.Alm.Present_Value - ns=2;s=System1.OPC:OPC.F03.A.Ahu101.Alm.Present_Value */

static UA_StatusCode function_Hvac_Simulation_NodeSet_9_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 6LU);
UA_Int32 *variablenode_ns_2_s_system1_opc_opc_f03_a_ahu101_alm_present_value_variant_DataContents =  UA_Int32_new();
if (!variablenode_ns_2_s_system1_opc_opc_f03_a_ahu101_alm_present_value_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Int32_init(variablenode_ns_2_s_system1_opc_opc_f03_a_ahu101_alm_present_value_variant_DataContents);
*variablenode_ns_2_s_system1_opc_opc_f03_a_ahu101_alm_present_value_variant_DataContents = (UA_Int32) 1;
UA_Variant_setScalar(&attr.value, variablenode_ns_2_s_system1_opc_opc_f03_a_ahu101_alm_present_value_variant_DataContents, &UA_TYPES[UA_TYPES_INT32]);
attr.displayName = UA_LOCALIZEDTEXT("", "System1.OPC:OPC.F03.A.Ahu101.Alm.Present_Value");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "System1.OPC:OPC.F03.A.Ahu101.Alm.Present_Value");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[2], "System1.OPC:OPC.F03.A.Ahu101.Alm.Present_Value"),
UA_NODEID_STRING(ns[2], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[2], "System1.OPC:OPC.F03.A.Ahu101.Alm.Present_Value"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
if (retVal != UA_STATUSCODE_GOOD) return retVal;
UA_Int32_delete(variablenode_ns_2_s_system1_opc_opc_f03_a_ahu101_alm_present_value_variant_DataContents);
return retVal;
}

static UA_StatusCode function_Hvac_Simulation_NodeSet_9_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[2], "System1.OPC:OPC.F03.A.Ahu101.Alm.Present_Value")
);
}

/* System1.OPC:OPC.D02.A.Ahu201.Azo2.TR.Present_Value - ns=2;s=System1.OPC:OPC.D02.A.Ahu201.Azo2.TR.Present_Value */

static UA_StatusCode function_Hvac_Simulation_NodeSet_10_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.historizing = true;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_2_s_system1_opc_opc_d02_a_ahu201_azo2_tr_present_value_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_2_s_system1_opc_opc_d02_a_ahu201_azo2_tr_present_value_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_2_s_system1_opc_opc_d02_a_ahu201_azo2_tr_present_value_variant_DataContents);
*variablenode_ns_2_s_system1_opc_opc_d02_a_ahu201_azo2_tr_present_value_variant_DataContents = (UA_Double) 19.0787658691406;
UA_Variant_setScalar(&attr.value, variablenode_ns_2_s_system1_opc_opc_d02_a_ahu201_azo2_tr_present_value_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "System1.OPC:OPC.D02.A.Ahu201.Azo2.TR.Present_Value");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "System1.OPC:OPC.D02.A.Ahu201.Azo2.TR.Present_Value");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[2], "System1.OPC:OPC.D02.A.Ahu201.Azo2.TR.Present_Value"),
UA_NODEID_STRING(ns[2], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "System1.OPC:OPC.D02.A.Ahu201.Azo2.TR.Present_Value"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
if (retVal != UA_STATUSCODE_GOOD) return retVal;
UA_Double_delete(variablenode_ns_2_s_system1_opc_opc_d02_a_ahu201_azo2_tr_present_value_variant_DataContents);
return retVal;
}

static UA_StatusCode function_Hvac_Simulation_NodeSet_10_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[2], "System1.OPC:OPC.D02.A.Ahu201.Azo2.TR.Present_Value")
);
}

/* System1.OPC:OPC.D02.A.Ahu201.Azo1.TR.Present_Value - ns=2;s=System1.OPC:OPC.D02.A.Ahu201.Azo1.TR.Present_Value */

static UA_StatusCode function_Hvac_Simulation_NodeSet_11_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.historizing = true;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_2_s_system1_opc_opc_d02_a_ahu201_azo1_tr_present_value_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_2_s_system1_opc_opc_d02_a_ahu201_azo1_tr_present_value_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_2_s_system1_opc_opc_d02_a_ahu201_azo1_tr_present_value_variant_DataContents);
*variablenode_ns_2_s_system1_opc_opc_d02_a_ahu201_azo1_tr_present_value_variant_DataContents = (UA_Double) 20.0841674804688;
UA_Variant_setScalar(&attr.value, variablenode_ns_2_s_system1_opc_opc_d02_a_ahu201_azo1_tr_present_value_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "System1.OPC:OPC.D02.A.Ahu201.Azo1.TR.Present_Value");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "System1.OPC:OPC.D02.A.Ahu201.Azo1.TR.Present_Value");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[2], "System1.OPC:OPC.D02.A.Ahu201.Azo1.TR.Present_Value"),
UA_NODEID_STRING(ns[2], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "System1.OPC:OPC.D02.A.Ahu201.Azo1.TR.Present_Value"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
if (retVal != UA_STATUSCODE_GOOD) return retVal;
UA_Double_delete(variablenode_ns_2_s_system1_opc_opc_d02_a_ahu201_azo1_tr_present_value_variant_DataContents);
return retVal;
}

static UA_StatusCode function_Hvac_Simulation_NodeSet_11_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[2], "System1.OPC:OPC.D02.A.Ahu201.Azo1.TR.Present_Value")
);
}

/* System1.OPC:OPC.D02.A.Ahu200.TEx.Present_Value - ns=2;s=System1.OPC:OPC.D02.A.Ahu200.TEx.Present_Value */

static UA_StatusCode function_Hvac_Simulation_NodeSet_12_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.historizing = true;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_2_s_system1_opc_opc_d02_a_ahu200_tex_present_value_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_2_s_system1_opc_opc_d02_a_ahu200_tex_present_value_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_2_s_system1_opc_opc_d02_a_ahu200_tex_present_value_variant_DataContents);
*variablenode_ns_2_s_system1_opc_opc_d02_a_ahu200_tex_present_value_variant_DataContents = (UA_Double) 18.8894958496094;
UA_Variant_setScalar(&attr.value, variablenode_ns_2_s_system1_opc_opc_d02_a_ahu200_tex_present_value_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "System1.OPC:OPC.D02.A.Ahu200.TEx.Present_Value");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "System1.OPC:OPC.D02.A.Ahu200.TEx.Present_Value");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[2], "System1.OPC:OPC.D02.A.Ahu200.TEx.Present_Value"),
UA_NODEID_STRING(ns[2], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "System1.OPC:OPC.D02.A.Ahu200.TEx.Present_Value"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
if (retVal != UA_STATUSCODE_GOOD) return retVal;
UA_Double_delete(variablenode_ns_2_s_system1_opc_opc_d02_a_ahu200_tex_present_value_variant_DataContents);
return retVal;
}

static UA_StatusCode function_Hvac_Simulation_NodeSet_12_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[2], "System1.OPC:OPC.D02.A.Ahu200.TEx.Present_Value")
);
}

/* System1.OPC:OPC.D02.A.Ahu100.TEx.Present_Value - ns=2;s=System1.OPC:OPC.D02.A.Ahu100.TEx.Present_Value */

static UA_StatusCode function_Hvac_Simulation_NodeSet_13_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.historizing = true;
attr.minimumSamplingInterval = 0.000000;
attr.userAccessLevel = 1;
attr.accessLevel = 1;
/* Value rank inherited */
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_2_s_system1_opc_opc_d02_a_ahu100_tex_present_value_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_2_s_system1_opc_opc_d02_a_ahu100_tex_present_value_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_2_s_system1_opc_opc_d02_a_ahu100_tex_present_value_variant_DataContents);
*variablenode_ns_2_s_system1_opc_opc_d02_a_ahu100_tex_present_value_variant_DataContents = (UA_Double) 21.52099609375;
UA_Variant_setScalar(&attr.value, variablenode_ns_2_s_system1_opc_opc_d02_a_ahu100_tex_present_value_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "System1.OPC:OPC.D02.A.Ahu100.TEx.Present_Value");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "System1.OPC:OPC.D02.A.Ahu100.TEx.Present_Value");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[2], "System1.OPC:OPC.D02.A.Ahu100.TEx.Present_Value"),
UA_NODEID_STRING(ns[2], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "System1.OPC:OPC.D02.A.Ahu100.TEx.Present_Value"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
if (retVal != UA_STATUSCODE_GOOD) return retVal;
UA_Double_delete(variablenode_ns_2_s_system1_opc_opc_d02_a_ahu100_tex_present_value_variant_DataContents);
return retVal;
}

static UA_StatusCode function_Hvac_Simulation_NodeSet_13_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[2], "System1.OPC:OPC.D02.A.Ahu100.TEx.Present_Value")
);
}

UA_StatusCode Hvac_Simulation_NodeSet(UA_Server *server) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
/* Use namespace ids generated by the server */
UA_UInt16 ns[3];
ns[0] = UA_Server_addNamespace(server, "http://opcfoundation.org/UA/");
ns[1] = UA_Server_addNamespace(server, "http://siemens.com/opcuaDesignCD");
ns[2] = UA_Server_addNamespace(server, "http://siemens.com/DesigoCC/");

/* Load custom datatype definitions into the server */
if((retVal = function_Hvac_Simulation_NodeSet_0_begin(server, ns)) != UA_STATUSCODE_GOOD) return retVal;
if((retVal = function_Hvac_Simulation_NodeSet_1_begin(server, ns)) != UA_STATUSCODE_GOOD) return retVal;
if((retVal = function_Hvac_Simulation_NodeSet_2_begin(server, ns)) != UA_STATUSCODE_GOOD) return retVal;
if((retVal = function_Hvac_Simulation_NodeSet_3_begin(server, ns)) != UA_STATUSCODE_GOOD) return retVal;
if((retVal = function_Hvac_Simulation_NodeSet_4_begin(server, ns)) != UA_STATUSCODE_GOOD) return retVal;
if((retVal = function_Hvac_Simulation_NodeSet_5_begin(server, ns)) != UA_STATUSCODE_GOOD) return retVal;
if((retVal = function_Hvac_Simulation_NodeSet_6_begin(server, ns)) != UA_STATUSCODE_GOOD) return retVal;
if((retVal = function_Hvac_Simulation_NodeSet_7_begin(server, ns)) != UA_STATUSCODE_GOOD) return retVal;
if((retVal = function_Hvac_Simulation_NodeSet_8_begin(server, ns)) != UA_STATUSCODE_GOOD) return retVal;
if((retVal = function_Hvac_Simulation_NodeSet_9_begin(server, ns)) != UA_STATUSCODE_GOOD) return retVal;
if((retVal = function_Hvac_Simulation_NodeSet_10_begin(server, ns)) != UA_STATUSCODE_GOOD) return retVal;
if((retVal = function_Hvac_Simulation_NodeSet_11_begin(server, ns)) != UA_STATUSCODE_GOOD) return retVal;
if((retVal = function_Hvac_Simulation_NodeSet_12_begin(server, ns)) != UA_STATUSCODE_GOOD) return retVal;
if((retVal = function_Hvac_Simulation_NodeSet_13_begin(server, ns)) != UA_STATUSCODE_GOOD) return retVal;
if((retVal = function_Hvac_Simulation_NodeSet_13_finish(server, ns)) != UA_STATUSCODE_GOOD) return retVal;
if((retVal = function_Hvac_Simulation_NodeSet_12_finish(server, ns)) != UA_STATUSCODE_GOOD) return retVal;
if((retVal = function_Hvac_Simulation_NodeSet_11_finish(server, ns)) != UA_STATUSCODE_GOOD) return retVal;
if((retVal = function_Hvac_Simulation_NodeSet_10_finish(server, ns)) != UA_STATUSCODE_GOOD) return retVal;
if((retVal = function_Hvac_Simulation_NodeSet_9_finish(server, ns)) != UA_STATUSCODE_GOOD) return retVal;
if((retVal = function_Hvac_Simulation_NodeSet_8_finish(server, ns)) != UA_STATUSCODE_GOOD) return retVal;
if((retVal = function_Hvac_Simulation_NodeSet_7_finish(server, ns)) != UA_STATUSCODE_GOOD) return retVal;
if((retVal = function_Hvac_Simulation_NodeSet_6_finish(server, ns)) != UA_STATUSCODE_GOOD) return retVal;
if((retVal = function_Hvac_Simulation_NodeSet_5_finish(server, ns)) != UA_STATUSCODE_GOOD) return retVal;
if((retVal = function_Hvac_Simulation_NodeSet_4_finish(server, ns)) != UA_STATUSCODE_GOOD) return retVal;
if((retVal = function_Hvac_Simulation_NodeSet_3_finish(server, ns)) != UA_STATUSCODE_GOOD) return retVal;
if((retVal = function_Hvac_Simulation_NodeSet_2_finish(server, ns)) != UA_STATUSCODE_GOOD) return retVal;
if((retVal = function_Hvac_Simulation_NodeSet_1_finish(server, ns)) != UA_STATUSCODE_GOOD) return retVal;
if((retVal = function_Hvac_Simulation_NodeSet_0_finish(server, ns)) != UA_STATUSCODE_GOOD) return retVal;
return retVal;
}
