#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <pthread.h>

#include "open62541.h" 
// ----------------------- 1.)
#include "Hvac_Simulation_NodeSet.h"

/* Local definitions:
 * Usually, all test cases should be able to finish within 15 seconds. When test cases are finished, this
 * UA test server will be stopped normally.
 * However, if there are failed test case in the middle of testing, the test server may leave there and run forever.
 * In this case, one have to manually kill the process before one can launch another "make test". This is annoying 
 * and time consuming.
 * Since test case failed is quite often in development phase, here we set a time for the test server to stop 
 * itself after, say 180 seconds. 180 seconds should be enough to finish all test cases and correct some error and 
 * launch another test. If not, we can make it bigger.
 */
#define SLEEP_TIME_MS 1000
#define MAX_COUNT 180     // for development phase. Makesure if a test failed, server will not run forever

/* Structure definition for passing to the 
 * thread simulation routine. 
 * Server and namespace variables
 */
struct simThreadParams {
    UA_UInt16 *ns;
    UA_Server *server;
};

/* Global variable to enable / disbable the server */
static volatile UA_Boolean running = true;

/* Handler to trigger server stop.
 * Triggered by Ctrl+c.
 */
static void stopHandler(int sig) 
{
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "received ctrl-c");
    running = false;
}

/* Simulation routine. To be launched as a parallel thread */
void *simThreadFunc(void *ptr) 
{
    
    struct simThreadParams *threadParams = ptr;
    int utime = SLEEP_TIME_MS * 1000;
    int count = 0;
    int max_round = 0;
    UA_Variant value;
    UA_Double cntDouble;
    UA_Server *server = *(&threadParams->server);
    
    /* While the server is running */
    while(running) 
    {
        /* Increment count */    
	max_round++;
        count = !count;
	
        /* Cast value, assign variant, and write to server */ 
        //cntDouble = (UA_Double)count;
	//UA_Variant_setScalarCopy(&value, &cntDouble, &UA_TYPES[UA_TYPES_DOUBLE]);
	UA_Variant_setScalarCopy(&value, &count, &UA_TYPES[UA_TYPES_INT32]);
	UA_StatusCode wret =
        //UA_Server_writeValue(server, UA_NODEID_STRING((*(&threadParams->ns))[1], "System1.OPC:OPC.H06.A.Ahu200.FanEh.Cmd.Feedback01_Value"), value);
	UA_Server_writeValue(server, UA_NODEID_STRING((*(&threadParams->ns))[2], "System1.OPC:OPC.H06.A.Ahu200.FanEh.Cmd.Feedback01_Value"), value);        

        /* Sleep the thread */
        usleep(utime);

	/* for development only, turn this off before release. Test should be finished before run MAX_COUNT rounds.
 	* Test case failing is often in development phase, without this, if a test case failed, this server will run
 	* forever, and one have to manually kill the old server process before launch another "make test". */
	if(max_round  >  MAX_COUNT)      
		running = false;

	// check the "magic number"
	/*if(wret != UA_STATUSCODE_GOOD)
		running = false;*/

    }
    
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Exiting thread");
    return NULL;
}


int main(void) 
{
    signal(SIGINT, stopHandler);
    signal(SIGTERM, stopHandler);

    /* Create new server using default configuration */
    UA_Server *server = UA_Server_new();
    UA_ServerConfig_setDefault(UA_Server_getConfig(server));

    /* Use namespace ids generated by the server */
    UA_UInt16 ns[3];
    ns[0] = UA_Server_addNamespace(server, "http://opcfoundation.org/UA/");
    ns[1] = UA_Server_addNamespace(server, "http://siemens.com/opcuaDesignCD");
    ns[2] = UA_Server_addNamespace(server, "http://siemens.com/DesigoCC/");

#if 0
    ns[0] = UA_Server_addNamespace(server, "http://opcfoundation.org/UA/");
    ns[1] = UA_Server_addNamespace(server, "http://siemens.com/DesigoCC/");
    ns[2] = UA_Server_addNamespace(server, "http://www.prosysopc.com/OPCUA/SampleAddressSpace");
#endif 

    /* Add simulation NodeSet (subset of CFHVMS nodes) --- 2.) */
    UA_StatusCode retval = Hvac_Simulation_NodeSet(server);
    
    pthread_t threadSim;
    int ret = 0;
    
    /* Create struct to pass to threaded simulation routine 
     * Contains server and namespace variables 
     */ 
    struct simThreadParams threadParams;
    threadParams.server = server;
    threadParams.ns = ns;
    
    /* Create nodes from nodeset */
    if(retval != UA_STATUSCODE_GOOD) 
    {
        UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Could not add the example nodeset. "
            "Check previous output for any error.");
        retval = UA_STATUSCODE_BADUNEXPECTEDERROR;
    } else {
        ret = pthread_create( &threadSim, NULL, simThreadFunc, &threadParams);
        if(ret) 
	{
            UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Error - pthread_create(): %d", ret);
            exit(EXIT_FAILURE);
        }
        /* Main server loop. Will remain in this function
         * while running = true 
         */  
        retval = UA_Server_run(server, &running);
    }

    /* Give thread enough time to exit */
    usleep(SLEEP_TIME_MS * 1000);

    /* Cleanup and return */
    UA_Server_delete(server);
    return retval == UA_STATUSCODE_GOOD ? EXIT_SUCCESS : EXIT_FAILURE;
}
